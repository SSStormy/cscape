﻿namespace CScape.Core.Network.Sync
{
    public static class SyncMachineConstants
    {
        public const int Region = 0;
        public const int Observer = 1;
        public const int PlayerUpdate = 2;
        public const int NpcUpdate = 3;
        public const int GroundItemSync = 4;
        public const int Interface = 5;
        public const int Skills = 6;
        public const int Message = 7;
        public const int DebugStat = 8;
    }
}
