namespace CScape.Core.Game.Interface
{
    public interface ISidebarInterface : IBaseInterface
    {
        int SidebarIndex { get; }
    }
}