namespace CScape.Core.Game.Interface
{
    public enum InterfaceType
    {
        Main,
        Sidebar,
        Chat,
        Input
    }
}