using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace CScape.Core.Game.Entity
{
    public class EntityPool<T> : IEnumerable<T> where T : class, IEntity
    {
        public int Size => _pool.Count;

        // the pool CANNOT contain null keys.
        private readonly Dictionary<uint, T> _pool;

        public EntityPool()
        {
            _pool = new Dictionary<uint, T>();
        }

        [CanBeNull]
        public T GetById(uint id)
        {
            if (!_pool.ContainsKey(id))
                return null;

            return _pool[id];
        }

        public void Add([NotNull] T ent)
        {
            if (ent == null) throw new ArgumentNullException(nameof(ent));
            Debug.Assert(!_pool.ContainsKey(ent.UniqueEntityId));

            _pool[ent.UniqueEntityId] = ent;
        }

        public void Remove([NotNull] T ent)
        {
            if (ent == null) throw new ArgumentNullException(nameof(ent));

            Remove(ent.UniqueEntityId);
        }

        public void Remove(uint id)
        {
            if(!_pool.ContainsKey(id))
                return;

            Debug.Assert(_pool[id] != null);
            _pool.Remove(id);
        }

        public bool ContainsId(uint id)
            => _pool.ContainsKey(id);

        [DebuggerStepThrough]
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        [DebuggerStepThrough]
        public IEnumerator<T> GetEnumerator()
        {
            foreach (var p in _pool.Values)
            {
                Debug.Assert(p != null);
                yield return p;
            }
        }
    }
}