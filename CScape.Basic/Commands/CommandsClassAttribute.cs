using System;

namespace CScape.Basic.Commands
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class CommandsClassAttribute : Attribute { }
}