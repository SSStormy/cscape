namespace CScape.Basic
{
    public enum LogSeverity
    {
        Debug,
        Normal,
        Warning,
        Exception
    }
}